package main

import (
	"html/template"
	"io"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/shootersf/contacts-htmx/controller"
	"gitlab.com/shootersf/contacts-htmx/handler"
)

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func newTemplate() *Template {
	funcMap := template.FuncMap{
		"dec": func(a int) int { return a - 1 },
		"inc": func(a int) int { return a + 1 },
		"mul": func(a, b float64) float64 { return a * b },
	}

	return &Template{
		templates: template.Must(template.New("").Funcs(funcMap).ParseGlob("view/*.html")),
	}
}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())

	e.Renderer = newTemplate()
	e.Static("/static", "static")

	h := handler.NewHomeHandler()

	// Initialise "contacts" and "archiver"
	controller.ContactsInit()
	controller.ArchiverInit()

	e.GET("/", h.HandleIndexPage)
	e.GET("/contacts", h.HandleContactsPage)
	e.GET("/contacts/new", h.HandleCreateContactPage)
	e.GET("/contacts/count", h.HandleContactCount)
	e.POST("/contacts/new", h.HandleContactCreation)
	e.POST("/contacts/delete", h.HandleContactsDeletion)
	e.GET("/contacts/new/email", h.HandleNewEmailValidation)
	e.GET("/contacts/:id", h.HandleContactPage)
	e.GET("/contacts/:id/edit", h.HandleEditContactPage)
	e.GET("/contacts/:id/email", h.HandleEditEmailValidation)
	e.POST("/contacts/:id/edit", h.HandleContactUpdate)
	e.DELETE("/contacts/:id", h.HandleContactDeletion)
	e.POST("/contacts/archive", h.HandleArchiveStart)
	e.GET("/contacts/archive", h.HandleArchivePolling)
	e.DELETE("/contacts/archive", h.HandleArchiveCancel)

	e.Logger.Fatal(e.Start(":42420"))
}
