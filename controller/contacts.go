package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
)

type Contact struct {
	Name  string
	Email string
	Id    int
}

var id = 0

func newContact(name string, email string) Contact {
	id++
	return Contact{
		Name:  name,
		Email: email,
		Id:    id,
	}
}

type Contacts struct {
	Contacts []Contact
}

type ContactErrors struct {
	Name  string
	Email string
}

type FormData struct {
	Name   string
	Email  string
	Errors ContactErrors
}

type FormDataWithId struct {
	FormData
	Id int
}

var storedContacts Contacts

func ContactsInit() {
	storedContacts = Contacts{
		Contacts: []Contact{
			newContact("John Smith", "john_smith@yahoo.com"),
			newContact("Lisa Johnson", "lisa123@hotmail.com"),
			newContact("Emily Brown", "emily.brown@gmail.com"),
			newContact("Michael Davis", "michael_davis@outlook.com"),
			newContact("Robert Wilson", "robert_wilson@gmail.com"),
			newContact("Jessica Lee", "jessica.lee@yahoo.com"),
			newContact("David Thompson", "davidt@gmail.com"),
			newContact("Jennifer Martinez", "jenni.martinez@yahoo.com"),
			newContact("Matthew Taylor", "matt_taylor@hotmail.com"),
			newContact("Olivia Harris", "olivia123@gmail.com"),
			newContact("Daniel Anderson", "daniel_anderson@yahoo.com"),
			newContact("Ava Clark", "ava.c@hotmail.com"),
			newContact("William White", "will_white@gmail.com"),
			newContact("Sophia Hall", "sophieh@yahoo.com"),
			newContact("James Miller", "james.miller@hotmail.com"),
			newContact("Charlotte Brown", "charlotte.brown@gmail.com"),
			newContact("Benjamin Jackson", "ben_jackson@yahoo.com"),
			newContact("Amelia Thomas", "amelia.t@hotmail.com"),
			newContact("Ethan Garcia", "ethan_garcia@gmail.com"),
			newContact("Mia Rodriguez", "mia.r@yahoo.com"),
			newContact("Alexander Martinez", "alex.martinez@hotmail.com"),
			newContact("Harper Hernandez", "harper.h@gmail.com"),
			newContact("Aiden Gonzalez", "aiden.g@yahoo.com"),
			newContact("Sofia Perez", "sofia.p@hotmail.com"),
			newContact("Jackson Adams", "jackson.adams@gmail.com"),
			newContact("Evelyn Nelson", "evelyn.n@yahoo.com"),
			newContact("Mason Carter", "mason.c@hotmail.com"),
			newContact("Avery Torres", "avery_torres@gmail.com"),
			newContact("Scarlett Ramirez", "scarlett.r@yahoo.com"),
			newContact("Luke Evans", "luke.evans@hotmail.com"),
			newContact("Grace Wood", "grace.wood@gmail.com"),
			newContact("Carter Mitchell", "carter.m@yahoo.com"),
			newContact("Zoey Campbell", "zoey_c@hotmail.com"),
			newContact("Chloe Flores", "chloe.flores@gmail.com"),
			newContact("Sebastian Green", "seb_green@yahoo.com"),
			newContact("Riley Roberts", "riley_r@hotmail.com"),
			newContact("Lily Phillips", "lily.p@gmail.com"),
			newContact("Joe Bob", "jb@le.com"),
			newContact("Jim Jimmithy", "james_squared@iol.ie"),
			newContact("Liam Reed", "liam_reed@yahoo.com"),
		},
	}
}

func ContactsAll() *[]Contact {
	return &storedContacts.Contacts
}

func ContactsSearch(value string) *[]Contact {
	var filtered []Contact
	for _, contact := range storedContacts.Contacts {
		if strings.HasPrefix(strings.ToLower(contact.Name), strings.ToLower(value)) {
			filtered = append(filtered, contact)
		}
	}
	return &filtered
}

func getContactIndexByEmail(email string) int {
	for i, contact := range storedContacts.Contacts {
		if contact.Email == email {
			return i
		}
	}
	return -1
}

func emailIsUnique(email string) bool {
	return getContactIndexByEmail(email) == -1
}

func validateFormData(name string, email string, emailChanged bool) (*FormData, error) {
	if name == "" {
		formError := ContactErrors{
			Name: "No name supplied",
		}
		data := FormData{
			Name:   name,
			Email:  email,
			Errors: formError,
		}
		return &data, errors.New("failed to create")
	}

	if email == "" {
		formError := ContactErrors{
			Email: "No email supplied",
		}
		data := FormData{
			Name:   name,
			Email:  email,
			Errors: formError,
		}
		return &data, errors.New("failed to create")
	}

	if emailChanged && !emailIsUnique(email) {
		formError := ContactErrors{
			Email: "Email is not unique",
		}
		data := FormData{
			Name:   name,
			Email:  email,
			Errors: formError,
		}
		return &data, errors.New("failed to create")
	}

	return nil, nil
}

func ContactsAdd(name string, email string) (*FormData, error) {
	formData, err := validateFormData(name, email, true)
	if err != nil {
		return formData, err
	}

	// Success
	contact := newContact(name, email)
	storedContacts.Contacts = append(storedContacts.Contacts, contact)

	return nil, nil
}

func ContactUpdate(contact *Contact, name string, email string) (*FormDataWithId, error) {
	emailChanged := contact.Email != email
	formData, err := validateFormData(name, email, emailChanged)
	if err != nil {
		formDataWithId := FormDataWithId{
			FormData: *formData,
			Id:       contact.Id,
		}
		return &formDataWithId, err
	}

	// Validated
	contact.Name = name
	contact.Email = email

	spew.Dump(storedContacts.Contacts)

	return nil, nil
}

func ContactDelete(id int) {
	index := getContactIndexById(id)
	if index == -1 {
		return
	}

	storedContacts.Contacts = append(storedContacts.Contacts[:index], storedContacts.Contacts[index+1:]...)
}

func GetContactById(id int) (*Contact, error) {
	for i, contact := range storedContacts.Contacts {
		if contact.Id == id {
			// This was tricky and go have apparently updated it to fix the issue
			return &storedContacts.Contacts[i], nil
		}
	}
	return nil, errors.New("contact not found")
}

func getContactIndexById(id int) int {
	for i, contact := range storedContacts.Contacts {
		if contact.Id == id {
			return i
		}
	}
	return -1
}

func ContactVerifyEditEmail(id int, newEmail string) string {
	contactUpdating, _ := GetContactById(id)
	emailChanged := contactUpdating.Email != newEmail
	isUnique := emailIsUnique(newEmail)

	if emailChanged && !isUnique {
		return "Email is not unique"
	}

	return ""
}

func ContactVerifyNewEmail(newEmail string) string {
	if emailIsUnique(newEmail) {
		return ""
	}
	return "Email is not unique"
}

func GetContactsLength() int {
	return len(storedContacts.Contacts)
}

// Archiver Section //
type Archiver struct {
	cancel   chan bool
	Status   string
	Progress float64
}

var archiver Archiver

func ArchiverInit() {
	archiver = Archiver{
		Status:   "waiting",
		Progress: 0,
		cancel:   make(chan bool),
	}
}

func GetArchiver() *Archiver {
	return &archiver
}

func writeContactsToFile() {
	json, err := json.Marshal(storedContacts.Contacts)
	if err != nil {
		panic("Error marshalling json" + err.Error())
	}

	if err := os.WriteFile("static/lejson", json, 0666); err != nil {
		panic("Couldn't write file" + err.Error())
	}
}

func deleteContactsFile() {
	err := os.Remove("static/lejson")
	if err != nil && !os.IsNotExist(err) {
		panic("Couldn't delete file " + err.Error())
	}
}

func run(a *Archiver) {
	a.Status = "running"
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			a.Progress += 0.1
			fmt.Printf("Progress: %f\n", a.Progress)
			if a.Progress >= 1 {
				a.Status = "complete"
				writeContactsToFile()
				return
			}
		case <-a.cancel:
			a.Status = "waiting"
			a.Progress = 0
			return
		default:
			a.Status = "running"
		}
	}
}

func (a *Archiver) Start() {
	if a.Status != "waiting" {
		return
	}
	a.Status = "running"

	go run(a)
}

func (a *Archiver) Reset() {
	if a.Status == "running" {
		a.cancel <- true
	}
	if a.Status == "complete" {
		a.Status = "waiting"
		a.Progress = 0
	}
	deleteContactsFile()
}
