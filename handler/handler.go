package handler

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/labstack/echo/v4"
	"gitlab.com/shootersf/contacts-htmx/controller"
)

type HomeHandler struct{}

type ContactsData struct {
	Contacts *[]controller.Contact
	Archiver *controller.Archiver
	Search   string
	Page     int
	Prev     bool
	Next     bool
}

func NewHomeHandler() HomeHandler {
	return HomeHandler{}
}

func (h HomeHandler) HandleIndexPage(c echo.Context) error {
	return c.Redirect(http.StatusFound, "/contacts")
}

func (h HomeHandler) HandleContactsPage(c echo.Context) error {
	search := c.QueryParam("q")
	pageStr := c.QueryParam("page")
	if pageStr == "" {
		pageStr = "1"
	}
	page, err := strconv.Atoi(pageStr)
	if err != nil {
		return c.String(400, "Invalid Page Number")
	}

	var contacts *[]controller.Contact

	if search == "" {
		contacts = controller.ContactsAll()
	} else {
		contacts = controller.ContactsSearch(search)
	}

	pageSize := 10

	startIndex := (page - 1) * pageSize
	endIndex := startIndex + pageSize

	if endIndex > len(*contacts) {
		endIndex = len(*contacts)
	}

	paginatedContacts := (*contacts)[startIndex:endIndex]

	archiver := controller.GetArchiver()

	data := ContactsData{
		Contacts: &paginatedContacts,
		Search:   search,
		Page:     page,
		Prev:     page != 1,
		Next:     len(*contacts) >= page*pageSize,
		Archiver: archiver,
	}

	// Active search handler
	triggerHeader := c.Request().Header.Get("HX-Trigger")
	if triggerHeader == "search" {
		time.Sleep(500 * time.Millisecond)
		return c.Render(http.StatusOK, "contacts-table-body", data)
	}
	return c.Render(http.StatusOK, "contacts-page", data)
}

func (h HomeHandler) HandleContactCount(c echo.Context) error {
	time.Sleep(2 * time.Second)
	contactLen := controller.GetContactsLength()
	response := "(" + strconv.Itoa(contactLen) + " total Contacts)"
	return c.String(http.StatusOK, response)
}

func (h HomeHandler) HandleCreateContactPage(c echo.Context) error {
	return c.Render(http.StatusOK, "new-contact", nil)
}

func (h HomeHandler) HandleEditContactPage(c echo.Context) error {
	idString := c.Param("id")
	fmt.Println(idString)
	id, err := strconv.Atoi(idString)
	if err != nil {
		return c.String(400, "Invalid ID")
	}

	contact, err := controller.GetContactById(id)
	if err != nil {
		return c.String(404, "No contact with matching ID found")
	}

	errors := controller.ContactErrors{}
	data := struct {
		Name   string
		Email  string
		Errors controller.ContactErrors
		Id     int
	}{
		Name:   contact.Name,
		Email:  contact.Email,
		Errors: errors,
		Id:     id,
	}

	return c.Render(http.StatusOK, "edit-contact", data)
}

func (h HomeHandler) HandleContactCreation(c echo.Context) error {
	name := c.FormValue("name")
	email := c.FormValue("email")
	data, err := controller.ContactsAdd(name, email)
	if err != nil {
		spew.Dump(data)
		return c.Render(http.StatusSeeOther, "new-contact", data)
	}

	return c.Redirect(http.StatusFound, "/contacts")
}

func (h HomeHandler) HandleContactUpdate(c echo.Context) error {
	name := c.FormValue("name")
	email := c.FormValue("email")
	idString := c.Param("id")
	fmt.Println(idString)
	id, err := strconv.Atoi(idString)
	if err != nil {
		return c.String(400, "Invalid ID")
	}

	contact, err := controller.GetContactById(id)
	if err != nil {
		return c.String(404, "No contact with matching ID found")
	}

	data, err := controller.ContactUpdate(contact, name, email)
	if err != nil {
		return c.Render(http.StatusSeeOther, "edit-contact", data)
	}
	// Success
	return c.Redirect(http.StatusFound, "/contacts")
}

func (h HomeHandler) HandleContactDeletion(c echo.Context) error {
	idString := c.Param("id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		return c.String(400, "Invalid ID")
	}

	controller.ContactDelete(id)

	triggerHeader := c.Request().Header.Get("HX-Trigger")
	if triggerHeader == "delete-btn" {
		return c.Redirect(http.StatusSeeOther, "/contacts")
	}
	return c.NoContent(http.StatusOK)
}

func (h HomeHandler) HandleContactsDeletion(c echo.Context) error {
	if err := c.Request().ParseForm(); err != nil {
		return c.String(http.StatusBadRequest, "Form data issue")
	}
	form := c.Request().Form
	stringIds := form["selected_contacts_ids"]

	for _, stringId := range stringIds {
		id, err := strconv.Atoi(stringId)
		if err != nil {
			continue
		}
		controller.ContactDelete(id)
	}

	return h.HandleContactsPage(c)
}

func (h HomeHandler) HandleContactPage(c echo.Context) error {
	idString := c.Param("id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		return c.String(400, "Invalid ID")
	}

	contact, err := controller.GetContactById(id)
	if err != nil {
		return c.String(404, "No contact with matching ID found")
	}

	return c.Render(http.StatusOK, "single-contact-page", contact)
}

func (h HomeHandler) HandleEditEmailValidation(c echo.Context) error {
	idString := c.Param("id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		return c.String(400, "Invalid ID")
	}
	updatedEmail := c.QueryParam("email")
	emailError := controller.ContactVerifyEditEmail(id, updatedEmail)

	return c.Render(http.StatusOK, "edit-email-error", emailError)
}

func (h HomeHandler) HandleNewEmailValidation(c echo.Context) error {
	email := c.QueryParam("email")
	emailError := controller.ContactVerifyNewEmail(email)

	return c.Render(http.StatusOK, "new-email-error", emailError)
}

// Archiver stuff

func (h HomeHandler) HandleArchiveStart(c echo.Context) error {
	archiver := controller.GetArchiver()
	archiver.Start()

	return c.Render(http.StatusOK, "archive-ui", archiver)
}

func (h HomeHandler) HandleArchivePolling(c echo.Context) error {
	archiver := controller.GetArchiver()

	return c.Render(http.StatusOK, "archive-ui", archiver)
}

func (h HomeHandler) HandleArchiveCancel(c echo.Context) error {
	archiver := controller.GetArchiver()
	archiver.Reset()

	return c.Render(http.StatusOK, "archive-ui", archiver)
}
