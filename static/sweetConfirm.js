function sweetConfirm(elt, config) {
  Swal.fire(config)
    .then((result) => {
      if (result.isConfirmed) {
        //document.getElementById('delete-button').dispatchEvent(new Event('confirmed'))
        htmx.trigger(elt, 'confirmed')
      }
    })
}

htmx.logger = function(elt, event, data) {
  if(console) {
    console.log(event, elt, data);
  }
}
